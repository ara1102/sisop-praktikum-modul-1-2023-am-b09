#!/bin/bash

echo "Login Dashboard"

while true
do

    # Mengoutputkan prompt untuk meminta username dan sekaligus meminta input username
    read -p "Username:" username

    # Mengecek apakah username terdaftar
    isExist=$(awk -v user="$username" 'BEGIN { isExist = 0 } { if ( $1 == user ) isExist=1 } END { if (isExist) { print 1 } else { print 0 } }' users/users.txt)

    if [ $isExist == 0 ]
    then 
        # Apabila username belum terdaftar, maka sistem akan memberikan prompt untuk meminta input username kembali
        echo "Username doesn't exist, please input your username again"
        continue
    fi

    # Mengecek password
    while true
    do

        # Mengoutputkan prompt untuk meminta password dan sekaligus meminta input password
        read -p "Password:" password

        # Mengecek apakah password yang di inputkan benar
        isCorrect=$(awk -v user="$username" -v pass="$password" 'BEGIN { isCorrect = 0 } { if ( $1 == user && $2 == pass ) isCorrect=1 } END { if (isCorrect) { print 1 } else { print 0 } }' users/users.txt)
        
        if [ $isCorrect == 0 ]
        then 
            # Apabila password salah, sistem akan mengoutputkan message ke log.txt dan akan mencoba meminta input password kembali
            echo `date "+%Y/%m/%d %H:%M:%S"` "LOGIN: ERROR Failed login attempt on user $username" >> log.txt
            echo "Wrong password, please try again"
            continue
        fi
        
        # Apabila password benar, sistem akan mengoutputkan message ke log.txt
        echo `date "+%Y/%m/%d %H:%M:%S"` "LOGIN: INFO User $username logged in" >> log.txt
        break
        
    done

    break

done