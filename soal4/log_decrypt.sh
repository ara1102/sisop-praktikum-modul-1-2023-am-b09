#!/bin/bash

inputDirectory="/home/adhira/Documents/sisop/modul1/soal4/encrypt/"`date "+%H:%M %d:%m:%Y"`".txt"
upperCase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
lowerCase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"

shift=$(date "+%H")
directory="/home/adhira/Documents/sisop/modul1/soal4/decrypt/"`date "+%H:%M %d:%m:%Y"`".txt"

tr "${lowerCase:${shift}:26}" "${lowerCase:0:26}" < "$inputDirectory" | tr "${upperCase:${shift}:26}" "${upperCase}:0:26" >  "$directory"

#Crontab Script
# 0 */2 * * * home/adhira/Documents/sisop/modul1/soal4/log_decrypt.sh







