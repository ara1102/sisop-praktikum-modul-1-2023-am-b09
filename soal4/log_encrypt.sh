#!/bin/bash

upperCase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
lowerCase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"

shift=$(date "+%H")
directory="/home/adhira/Documents/sisop/modul1/soal4/encrypt/"`date "+%H:%M %d:%m:%Y"`".txt"

tr "${lowerCase:0:26}" "${lowerCase:${shift}:26}" < /var/log/syslog | tr "${upperCase:0:26}" "${upperCase:${shift}:26}" > "$directory"

#Crontab Script
# 0 */2 * * * home/adhira/Documents/sisop/modul1/soal4/log_encrypt.sh
