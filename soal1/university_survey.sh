#!/bin/bash

printf "1.Tampilkan universitas di jepang dengan ranking 5 teratas:\n"
awk 'BEGIN{FS=","}/Japan/ {print $2}' soalnomor1.csv | head -5

printf  "\n2. Tampilkan universitas di jepang dengan fsr score terendah dari soal di atas:\n"
awk 'BEGIN{FS=","}/Japan/ {print $2","$9}' soalnomor1.csv | sort  -g -t',' -k2| head -5 | cut -d',' -f1

printf  "\n3. Tampilkan universitas di jepang dengan employment outcome rank 10 tertinggi:\n"
awk 'BEGIN{FS=","}/Japan/ {print $2","$20}' soalnomor1.csv | sort -t',' -k2,2n | cut -d',' -f1 | head -10

printf  "\n4. Tampilkan universitas terkeren di dunia:\n"
awk 'BEGIN{FS=","}/Keren/ {print $2}' soalnomor1.csv
