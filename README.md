# sisop-praktikum-modul-1-2023-AM-B09

## Kelompok BO9 - Sistem Operasi B
| **No** | **Nama** | **NRP** | 
| :-------------: | ------------- | :---------: |
| 1 | Adhira Riyanti Amanda  | 5025211102 | 
| 2 | Moh Adib Syambudi | 5025211017 | 
| 3 | Mohammad Ahnaf Fauzan| 5025211170 |

## Soal No 1
**soal a**

Tampilkan 5 universitas  dengan ranking tertinggi di jepang

``` sh
awk 'BEGIN{FS=","}/Japan/ {print $2}' soalnomor1.csv | head -5
```
Penjelasan:
- awk diawal berfungsi untuk menjadikan text file sebagai records dan fields
- BEGIN berfungsi untuk mengawali tubuh program
- {FS=","} digunakan untukk menandai pemisah kolom dengan ,
- /japan/ digunakan untuk menampilkan kolom yang memiliki kata japan
- {print $2} fungsi print disini untuk menamiplkan kolom dari kolom ke dua 
- soalnomor1.csv artinya fungsi fungsi tersebut digunakan pada file
- head -5 digunakan untuk menampilkan 5 hasil dari atas karena disoal ranking sudah urut sehingga hanya perlu menampilkan 5 teratas

**soal b**

Tampilkan universitas  dengan fsr score 5 terendah dijepang
``` sh
awk 'BEGIN{FS=","}/Japan/ {print $2","$9}' soalnomor1.csv | head -5 | sort -g -t"," -k2| head -5| cut -d',' -f1
```
Penjelasan:
- awk diawal berfungsi untuk menjadikan text file sebagai records dan fields
- BEGIN berfungsi untuk mengawali tubuh program
- {FS=","} digunakan untukk menandai pemisah kolom dengan ,
- /japan/ digunakan untuk menampilkan kolom yang memiliki kata japan
- {print $2","$9} fungsi print disini untuk menamiplkan kolom dari kolom ke dua dan sembilan
- soalnomor1.csv artinya fungsi fungsi tersebut digunakan pada file
- sort -g -t"," -k2 untuk mengurutkan kolom kedua (fsr score) secara dari paling kecil
- head -5 untuk menampilkan 5 baris paling atas dari kolom
- cut -d',' -f1 untuk memotong dan menyisakan kolom ke 1 yaitu $2 sehingga hasilnya akan berupa nama suatu universitas


**soal c**

Tampilkan 10 universitas  dengan employment outcome ranking tertinggi di jepang

``` sh
awk 'BEGIN{FS=","}/Japan/ {print $2","$20}' soalnomor1.csv | sort -t',' -k2,2n | head -10 | cut -d',' -f1
```
Penjelasan:
- awk diawal berfungsi untuk menjadikan text file sebagai records dan fields
- BEGIN berfungsi untuk mengawali tubuh program
- {FS=","} digunakan untukk menandai pemisah kolom dengan ,
- /japan/ digunakan untuk menampilkan kolom yang memiliki kata japan
- {print $2","$20} fungsi print disini untuk menamiplkan kolom dari kolom ke dua dan dua puluh
- soalnomor1.csv artinya fungsi fungsi tersebut digunakan pada file
- sort -t',' -k2,2n untuk mengurutkan berdasarkan employment outcome ranking tertinggi dari kolom kedua 
- cut -d',' -f1 untuk memotong dan menyisakan kolom ke 1 yaitu $2 sehingga hasilnya akan berupa nama suatu universitas
- head -10 untuk menampilkan 10 baris data dari paling atas

**soal d**

Tampilkan universitas terkeren di dunia

``` sh
awk 'BEGIN{FS=","}/Keren/ {print $2}' soalnomor1.csv 
```
Penjelasan:
- awk diawal berfungsi untuk menjadikan text file sebagai records dan fields
- BEGIN berfungsi untuk mengawali tubuh program
- {FS=","} digunakan untukk menandai pemisah kolom dengan ,
- /Keren/ digunakan untuk menampilkan kolom yang memiliki kata kunci Keren
- {print $2} fungsi print disini untuk menamiplkan kolom dari kolom ke dua 
- soalnomor1.csv artinya fungsi fungsi tersebut digunakan di file ini
## Soal No 2
**soal a**

Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan.

Ketentuan

-File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
-File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 
```
declare -i now=`date +"%H"`
create_direc=$now
zip_ntim=$now
del_direc=$now

declare -i i=1

declare -i k=1

```
•Terdapat 4 buah variable yang digunakan untuk menyimpan variable waktu yang didapatkan melalui `date + “%H”` supaya mendapatkan jamnya. Variable awal tersebut juga di deklarasikan sebagai bentuk integer. Agar Ketika mengambil nilai delapan tidak terkena masalah format bilangan oktal

•	Membuat dua buah variable sebagai index banyak iterasi yang dilakukan. Variabel ini juga dideklarasikan dalam bentuk integer

```
while [ $create_direc -gt 0 ]
do
        mkdir "kumpulan_$k"
        k=$(( k+1 ))
	create_direc=$(( create_direc-1 ))
done
```
•	Langkah pertama dibuatkan directory sebanyak X jam. Untuk membuatnya dilakukan looping menggunakan while. Pengerjaan didalamnya `mkdir  “kumpulan_$k”` . kemudian variable k di increment untuk penyesuaian penamaan directory. Dan variable `create_direc`=$(( create_direc-1 )) untuk menjaga iterasi tidak infinite

•	Kemudian kita perlu menangkap directory active sebagai nanti memindahkan file perjalanan. `pwd` untuk mendapatkan directory tersebut. Serta dibuat variable `target_dir=”kumpulan_$i”` untuk folder tujuan.

```
if [ $now -eq 0 ]
then
	wget -O "perjalanan_$1" https://source.unsplash.com/random/?indonesia

	mv "perjalanan_1" "$current_path/kumpulan_1"

fi

```

•	Terdapat kasus yang berbeda oleh karena itu dibuatlah percabangan untuk Ketika jam 00 atau Ketika tengah malam. Maka dari itu jika variable now bernilai 0 akan langsung dibuatkan file dan folder dengan format perjalanan_1 dan kumpulan_1

```
while [ $now -gt 0 ]
do
	wget -O "perjalanan_$i" https://source.unsplash.com/random/?indonesia

	mv "perjalanan_$i" "$current_path/$target_dir"

	now=$(( now-1 ))
	i=$(( i+1 )) 
	target_dir="kumpulan_$i"
done

```

•	Dan kasus kedua diproses secara looping menggunakan while dengan jumlah iterasi sebanyak nilai variable now dengan cara decrement nilainya.Setiap iterasi akan dikurangi nilai dari variabel now dan nilai index akan di increment sesuai jam yang eksekusi yang telah ditentukan sebelumnya

•	Dari kedua proses tersebut kita menggunakan command `wget` dan link yang digunakan adalah https://source.unsplash.com/random?indonesia kita juga menambahkan parameter -O yang digunakan sebagai penamaan hasil output. Format penamaan adalah perjalanan_1 dst

•	Selama proses juga dipindahkan setiap folder yang telah di download ke directory yang dituju yaitu kumpulan_1 dst menggunakan command `mv`

•	Kode diharapkan untuk bisa di eksekusi secara periodic.  Adapun periode dalam eksekusi setiap 10 jam sekali. Untuk memenuhi hal tersebut maka dilakukan command `crontab -e`. dari situ kitab isa mengedit crontab menjadi sesuai kebutuhan kita melihat proses crontab 

**soal b**

Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst)

```
while [ $zip_ntim -gt 0 ]
do
        zip -r "devil_$j.zip" "kumpulan_$j"
        j=$(( j+1 ))
        zip_ntim=$(( zip_ntim - 1 ))
done

```

•	 Proses berikutnya adalah mengcompress folder kumpulan menjadi format zip dengan penamaan `”devil_1.zip”` menggunakan commad `zip`. terdapat opsi -r ditujukan untuk melakukan rekursi zip karena didalam directory mengandung sebuah file.

- Karena terdapat dua bagian segmen yang dijalankan dengan periode yang berbeda maka untuk mengtasi hal tersebut dibuat pembatas dengan menggunakan 
```
echo "#########################################"
```
-Dari situ dibuatlah dua buah pengaturan crontab yang mengarah pada file dan directory yang sama. crontab yang atas ditujukan untuk segmen pertama pada bagian atas echo. dan untuk segmen kedua akan menggunakan crontab yang kedua
`0 */10 * * * /home/adib/praktikum/soal2/kobeni_liburan.sh`

-Mengatur menjadi 10 jam sekali eksekusi

`0 17 * * * /home/adib/praktikum/soal2/kobeni_liburan.sh`

-Revisi: Mengatur menjadi dieksekusi pada setiap jam 17 sesuai running awal. Dimana pada soal diminta untuk menjalankan tiap 24 jam

`chmod +x kobeni_liburan.sh`

-Memastikan untuk mengizinkan akses dari file kobeni_liburan.sh bisa dieksekusi


## Soal No 3

**Soal:** 
Membuat sistem register dan login

    Ketentuan password sebagai berikut:
    1. Minimal 8 karakter
    2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
    3. Alphanumeric
    4. Tidak boleh sama dengan username 
    5. Tidak boleh menggunakan kata chicken atau ernie

    Ketentuan MESSAGE pada log:
    1. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
    2. Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
    3. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
    4. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in
- Log Format : YY/MM/DD hh:mm:ss MESSAGE
- File untuk menyimpan username dan password yang terdaftar: [/users/users.txt](/soal3/users/users.txt)
- File log : [log.txt](/soal3/log.txt)

**Pembahasan:**
Bentuk umum script [louis.sh](/soal3/louis.sh) dan [retep.sh](/soal3/retep.sh)

``` sh
while [condition] 
do
    Kode untuk mengecek input username
    .
    .
    .

    while [condition]
    do

         Kode untuk mengecek input password
         .
         .
         .

        break

    done
    break
done
```
#### Sistem registrasi : [louis.sh](/soal3/louis.sh)

**Mengecek input username**
``` sh
    while true
    do
        # Mengoutputkan prompt untuk menentukan username dan sekaligus meminta input username
        read -p "Set up your username:" username

        # Mengecek apakah username sudah terdaftar sebelumnya
        isExist=$(awk -v user="$username" 'BEGIN { isExist = 0 } { if ( $1 == user ) isExist=1 } END { if (isExist) { print 1 } else { print 0 } }' users/users.txt)

        if [ $isExist == 1 ]
        then 
            # Apabila username sudah terdaftar, maka sistem akan mengoutputkan message ke log.txt
            echo `date "+%Y/%m/%d %H:%M:%S"` "REGISTER: ERROR User already exists" >> log.txt
            continue
        fi

        # Flag untuk pengecekan input password
        isWrong=0
    
        #Kode untuk mengecek input password
        .
        .
        .
```
Potongan kode ini di wrap dengan sebuah while-loop sehingga ketika username sudah terdaftar, sistem akan meminta user untuk memasukan username baru hingga user menginput username yang belum terdaftarkan

Pengecekan username dilakukan dengan mengecek apakah input username terdapat pada kolom 1 dari file [/users/users.txt](/soal3/users/users.txt).
- Jika ada, sistem akan mengoutputkan message ke [log.txt](/soal3/log.txt) sesuai dengan ketentuan. Kemudian, sistem akan melakukan continue sehingga while-loop mengulang dan akan meminta user untuk memasukan username baru
- Jika tidak ada, sistem akan lanjut ke pengecekan input password 

**Mengecek input password**

```sh
        while [ $isWrong == 0 ]
        do
            # Mengoutputkan prompt untuk menentukan password dan sekaligus meminta input password
            read -p "Set up your password:" password

            # Mengecek apakah panjang password kurang dari mininimum
            if [ ${#password} -lt 8 ]
            then
                echo "The minimum length of the password is 8"
                isWrong=1
            fi

            # Mengecek apakah password memiliki uppercase, lowercase, dan angka
            if ! [[ $password =~ [A-Z] && $password =~ [a-z] && $password =~ [0-9] ]] 
            then
                echo "The password should contain at least one uppercase, one lowercase, and one number"
                isWrong=1
            fi

            # Mengecek apakah password hanya terdiri dari karakter alphanumerical
            if [[ $password =~ [^a-zA-Z0-9] ]]
            then
                echo "The password should be alphanumerical"
                    isWrong=1
            fi

            # Mengecek apakah password sama dengan username
            if [ $password == $username ]
            then
                echo "The password can't be the same as the username"
                isWrong=1
            fi

            # Mengecek apakah password mengandung kata "chicken" atau "ernie"
            if [[ $password == *chicken* ||  $password == *ernie* ]]
            then
                echo "The password couldn't contain the word \"chicken\" or \"ernie\""
                isWrong=1
            fi

            # Mengecek apakah flag isWrong bernilai true, jika iya maka user akan diminta untuk menginputkan ulang password seusai dengan ketentuan
            if [ $isWrong == 1 ]
            then
                isWrong=0
                continue
            fi

            # Apabila semua kondisi sudah terpenuhi, maka register berhasil dan mengoutputkan message ke log.txt
            echo `date "+%Y/%m/%d %H:%M:%S"` "REGISTER: INFO User $username registered successfully" >> log.txt

            # Username dan password yang sudah ter-register akan dioutputkan ke file users.txt
            echo "$username $password" >> users/users.txt
            break

        done

        break

    done
```
Setelah pengecekan input username. Sistem akan melakukan pengecekan password. Potongan kode pengecekan password di wrap dengan sebuah while-loop sehingga ketika password tidak sesuai dengan ketentuan, sistem akan meminta user untuk memasukan password kembali hingga user menginput password sesuai dengan ketentuan

Terdapat 5 hal yang di cek pada pengecekan password:
1. ``` sh
    # Mengecek apakah panjang password kurang dari mininimum
    if [ ${#password} -lt 8 ]
    then
        echo "The minimum length of the password is 8"
        isWrong=1
    fi
    ```
2.  ``` sh
    # Mengecek apakah password memiliki uppercase, lowercase, dan angka
    if ! [[ $password =~ [A-Z] && $password =~ [a-z] && $password =~ [0-9] ]] 
    then
        echo "The password should contain at least one uppercase, one lowercase, and one number"
        isWrong=1
    fi
    ```
3.  ``` sh
    # Mengecek apakah password hanya terdiri dari karakter alphanumerical
    if [[ $password =~ [^a-zA-Z0-9] ]]
    then
        echo "The password should be alphanumerical"
            isWrong=1
    fi
    ```
4.  ``` sh
    # Mengecek apakah password sama dengan username
    if [ $password == $username ]
    then
        echo "The password can't be the same as the username"
        isWrong=1
    fi
    ```
5.  ``` sh
    # Mengecek apakah password mengandung kata "chicken" atau "ernie"
        if [[ $password == *chicken* ||  $password == *ernie* ]]
        then
            echo "The password couldn't contain the word \"chicken\" or \"ernie\""
            isWrong=1
        fi
    ```
Ketika kondisi salah tersebut dicapai, sistem akan mengoutputkan pesan berisi mengapa password tersebut tidak dapat digunakan dan juga membuat nilai flag isWrong=1

Ketika flag isWrong bernilai 1, sistem akan melakukan continue sehingga while-loop mengulang dan akan meminta user untuk memasukan password baru sesuai dengan ketentuan
```sh
    # Mengecek apakah flag isWrong bernilai true, jika iya maka user akan diminta untuk menginputkan ulang password seusai dengan ketentuan
    if [ $isWrong == 1 ]
    then
        isWrong=0
        continue
    fi
```

Apabila semua kondisi password salah tidak terpenuhi, maka register berhasil dan sistem akan mengoutputkan message ke [log.txt](/soal3/log.txt). Kemudian sistem juga akan mengoutputkan username dan password yang sudah ter-register ke file [/users/users.txt](/soal3/users/users.txt).

```sh
    # Apabila semua kondisi password salah tidak terpenuhi, maka register berhasil dan mengoutputkan message ke log.txt
    echo `date "+%Y/%m/%d %H:%M:%S"` "REGISTER: INFO User $username registered successfully" >> log.txt

    # Username dan password yang sudah ter-register akan dioutputkan ke file users.txt
    echo "$username $password" >> users/users.txt
    break
```

#### Sistem login : [retep.sh](/soal3/retep.sh)

**Mengecek input username**

```sh
    while true
    do

        # Mengoutputkan prompt untuk meminta username dan sekaligus meminta input username
        read -p "Username:" username

        # Mengecek apakah username terdaftar
        isExist=$(awk -v user="$username" 'BEGIN { isExist = 0 } { if ( $1 == user ) isExist=1 } END { if (isExist) { print 1 } else { print 0 } }' users/users.txt)

        if [ $isExist == 0 ]
        then 
            # Apabila username belum terdaftar, maka sistem akan memberikan prompt untuk meminta input username kembali
            echo "Username doesn't exist, please input your username again"
            continue
        fi

        #Kode untuk mengecek input password
        .
        .
        .
```

Potongan kode ini di wrap dengan sebuah while-loop sehingga ketika input username belum terdaftar, sistem akan meminta user untuk memasukan username yang sudah terdaftar

Pengecekan username dilakukan dengan mengecek apakah input username tidak ada pada kolom 1 dari file [/users/users.txt](/soal3/users/users.txt).
- Jika tidak ada, sistem akan mengoutputkan pesan bahwa username tidak terdaftar dan meminta user untuk memasukan username kembali
- Jika ada, sistem akan lanjut ke pengecekan input password

**Mengecek input password**

```sh
        # Mengecek password
        while true
        do

            # Mengoutputkan prompt untuk meminta password dan sekaligus meminta input password
            read -p "Password:" password

            # Mengecek apakah password yang di inputkan benar
            isCorrect=$(awk -v user="$username" -v pass="$password" 'BEGIN { isCorrect = 0 } { if ( $1 == user && $2 == pass ) isCorrect=1 } END { if (isCorrect) { print 1 } else { print 0 } }' users/users.txt)
            
            if [ $isCorrect == 0 ]
            then 
                # Apabila password salah, sistem akan mengoutputkan message ke log.txt dan akan mencoba meminta input password kembali
                echo `date "+%Y/%m/%d %H:%M:%S"` "LOGIN: ERROR Failed login attempt on user $username" >> log.txt
                echo "Wrong password, please try again"
                continue
            fi
            
            # Apabila password benar, sistem akan mengoutputkan message ke log.txt
            echo `date "+%Y/%m/%d %H:%M:%S"` "LOGIN: INFO User $username logged in" >> log.txt
            break
            
        done

        break

    done
```
Setelah pengecekan input username. Sistem akan melakukan pengecekan password. Potongan kode ini di wrap dengan sebuah while-loop sehingga ketika input password tidak sesuai dengan password dari username yang disimpan di file [/users/users.txt](/soal3/users/users.txt), sistem akan meminta user untuk menginput passwordnya ulang sampai password tersebut benar.

- Ketika user salah memasukan password, sistem akan mengoutputkan message ke [log.txt](/soal3/log.txt) dan meminta user untuk menginput passwordnya ulang sampai benar
- Ketika user memasukan password yang benar, proses login berhasil dan sistem akan mengoutputkan message ke [log.txt](/soal3/log.txt)

## Soal No 4

**Soal:** 
Membuat program untuk melakukan backup log system komputer 

    Ketentuan backup file log system:
    1. Format file (jam:menit tanggal:bulan:tahun.txt).
    2. File tersebut di enkripsi dengan cara menggeser huruf alphabet pada file sebanyak jam pem-backup-an. 
    3. Membuat script untuk melakukan enkripsi dan dekripsinya.
    4. File syslog di backup setiap 2 jam 

### **Pembahasan:**

#### Script enkripsi : [log_encrypt.sh](/soal4/log_encrypt.sh)

```sh
#!/bin/bash

upperCase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
lowerCase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"

shift=$(date "+%H")
directory="/home/adhira/Documents/sisop/modul1/soal4/encrypt/"`date "+%H:%M %d:%m:%Y"`".txt"

tr "${lowerCase:0:26}" "${lowerCase:${shift}:26}" < /var/log/syslog | tr "${upperCase:0:26}" "${upperCase:${shift}:26}" > "$directory"

#Crontab Script
# 0 */2 * * * home/adhira/Documents/sisop/modul1/soal4/log_encrypt.sh
```

- `shift` : variabel untuk menyimpan data "hour" komputer saat melakukan backup. Data tersebut yang akan digunakan sebagai jumlah penggeseran huruf untuk enkripsi.
- `directory` : directory tempat file enkripsi akan disimpan. Output script ini akan disimpan disebuah folder bernama encrypt dengan nama file sesuai dengan ketentuan
- `tr` : digunakan untuk mentranslasi sebuah string sehingga masing masing huruf bergeser sebanyak `shift` sesuai dengan ketentuan enkripsi. 
    ```sh
    tr "${lowerCase:0:26}" "${lowerCase:${shift}:26}" < /var/log/syslog
    ```
    - Pada tr pertama dilakukan translasi untuk lowercase. Argumen pertama dari `tr` tersebut merupakan variabel `lowerCase` yang belum di translasi, sedangkan argumen kedua merupakan tujuan translasinya yaitu menggeser variabel `lowerCase` sebanyak `shift`
    - `< /var/log/syslog` merupakan input redirection ke file syslog yang akan di backup.

    ```sh
    | tr "${upperCase:0:26}" "${upperCase:${shift}:26}" > "$directory"
    ```
    - `|` berfungsi untuk menjadikan output perintah di sebelah kirinya sebagai input perintah disebelah kanannya. Oleh karena itu terdapat pipe diantara kedua perintah `tr`
    - Setelah melakukan translasi pada lowercase, output dari translasi tersebut akan dijadikan input untuk perintah translasi uppercase menggunakan `tr`.  Argumen pertama dari `tr` tersebut merupakan variabel `upperCase` yang belum di translasi, sedangkan argumen kedua merupakan tujuan translasinya yaitu menggeser variabel `upperCase` sebanyak `shift`
    - `> "$directory"` merupakan output redirection untuk menyimpan hasil output ke sebuah sebuah file pada suatu directory. string directory file tersebut disimpan pada variabel `directory` 

- `Crontab Script`
    ```sh
    # 0 */2 * * * home/adhira/Documents/sisop/modul1/soal4/log_encrypt.sh
    ```

    Perintah untuk menjalankan file [log_encrypt.sh](/soal4/log_encrypt.sh) setiap 2 jam sekali

#### Script dekripsi : [log_decrypt.sh](/soal4/log_decrypt.sh)

```sh
inputDirectory="/home/adhira/Documents/sisop/modul1/soal4/encrypt/"`date "+%H:%M %d:%m:%Y"`".txt"
upperCase="ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ"
lowerCase="abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"

shift=$(date "+%H")
directory="/home/adhira/Documents/sisop/modul1/soal4/decrypt/"`date "+%H:%M %d:%m:%Y"`".txt"

tr "${lowerCase:${shift}:26}" "${lowerCase:0:26}" < "$inputDirectory" | tr "${upperCase:${shift}:26}" "${upperCase}:0:26" >  "$directory"

#Crontab Script
# 0 */2 * * * home/adhira/Documents/sisop/modul1/soal4/log_decrypt.sh

```
- `inputDirectory`: string variabel yang menyimpan directory file di folder encrypt yang sudah dienkripsi sebelumnya
- `shift` : variabel untuk menyimpan data "hour" komputer saat melakukan backup. Data tersebut yang akan digunakan sebagai jumlah penggeseran huruf untuk dekripsi.
- `directory` : directory tempat file dekripsi akan disimpan. Output script ini akan disimpan disebuah folder bernama decrypt dengan nama file sesuai dengan ketentuan
- `tr` : digunakan untuk mentranslasi sebuah string sehingga huruf yang sudah bergeser ketika enkripsi akan bergeser kembali sebanyak `shift` seperti file syslog aslinya. 
    ```sh
    tr "${lowerCase:${shift}:26}" "${lowerCase:0:26}" < "$inputDirectory"
    ```
    - Pada tr pertama dilakukan translasi untuk lowercase. Argumen pertama dari `tr` tersebut merupakan variabel `lowerCase` yang sudah di translasi sebelumnya oleh script enkripsi sebanyak `shift`, sedangkan argumen kedua merupakan tujuan translasinya yaitu menggeser variabel `lowerCase` kembali
    - `< "$inputDirectory"` merupakan input redirection ke file yang disimpan pada variabel `inputDirectory`. File yang akan di input redirection ini merupakan file hasil enkripsi yang dilakukan sebelumnya

    ```sh
    | tr "${upperCase:0:26}" "${upperCase:${shift}:26}" > "$directory"
    ```
    - `|` berfungsi untuk menjadikan output perintah di sebelah kirinya sebagai input perintah disebelah kanannya. Oleh karena itu terdapat pipe diantara kedua perintah `tr`
    - Setelah melakukan translasi pada lowercase, output dari translasi tersebut akan dijadikan input untuk perintah translasi uppercase menggunakan `tr`.  Argumen pertama dari `tr` tersebut merupakan variabel `upperCase` yang sudah di translasi sebelumnya oleh script enkripsi sebanyak `shift`, sedangkan argumen kedua merupakan tujuan translasinya yaitu menggeser variabel `upperCase` kembali
    - `> "$directory"` merupakan output redirection untuk menyimpan hasil output ke sebuah sebuah file pada suatu directory. string directory file tersebut disimpan pada variabel `directory` 

- `Crontab Script`
    ```sh
    # 0 */2 * * * home/adhira/Documents/sisop/modul1/soal4/log_decrypt.sh
    ```

    Perintah untuk menjalankan file [log_decrypt.sh](/soal4/log_decrypt.sh) setiap 2 jam sekali

