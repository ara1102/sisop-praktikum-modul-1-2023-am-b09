#!/bin/bash

echo "Register Dashboard"

while true 
do
    # Mengoutputkan prompt untuk menentukan username dan sekaligus meminta input username
    read -p "Set up your username:" username

    # Mengecek apakah username sudah terdaftar sebelumnya
    isExist=$(awk -v user="$username" 'BEGIN { isExist = 0 } { if ( $1 == user ) isExist=1 } END { if (isExist) { print 1 } else { print 0 } }' users/users.txt)

    if [ $isExist == 1 ]
    then 
        # Apabila username sudah terdaftar, maka sistem akan mengoutputkan message ke log.txt
        echo `date "+%Y/%m/%d %H:%M:%S"` "REGISTER: ERROR User already exists" >> log.txt
        continue
    fi

    # Flag untuk pengecekan input password
    isWrong=0

    # Perulangan untuk mengecek input password pengguna agar sesuai dengan ketentuan password
    while [ $isWrong == 0 ]
    do

        # Mengoutputkan prompt untuk menentukan password dan sekaligus meminta input password
        read -p "Set up your password:" password

        # Mengecek apakah panjang password kurang dari mininimum
        if [ ${#password} -lt 8 ]
        then
            echo "The minimum length of the password is 8"
            isWrong=1
        fi

        # Mengecek apakah password memiliki uppercase, lowercase, dan angka
        if ! [[ $password =~ [A-Z] && $password =~ [a-z] && $password =~ [0-9] ]] 
        then
            echo "The password should contain at least one uppercase, one lowercase, and one number"
            isWrong=1
        fi

        # Mengecek apakah password hanya terdiri dari karakter alphanumerical
        if [[ $password =~ [^a-zA-Z0-9] ]]
        then
            echo "The password should be alphanumerical"
            isWrong=1
        fi

        # Mengecek apakah password sama dengan username
        if [ $password == $username ]
        then
            echo "The password can't be the same as the username"
            isWrong=1
        fi

        # Mengecek apakah password mengandung kata "chicken" atau "ernie"
        if [[ $password == *chicken* ||  $password == *ernie* ]]
        then
            echo "The password couldn't contain the word \"chicken\" or \"ernie\""
            isWrong=1
        fi

        # Mengecek apakah flag isWrong bernilai true, jika iya maka user akan diminta untuk menginputkan ulang password seusai dengan ketentuan
        if [ $isWrong == 1 ]
        then
            isWrong=0
            continue
        fi

        # Apabila semua kondisi password salah tidak terpenuhi, maka register berhasil dan mengoutputkan message ke log.txt
        echo `date "+%Y/%m/%d %H:%M:%S"` "REGISTER: INFO User $username registered successfully" >> log.txt

        # Username dan password yang sudah ter-register akan dioutputkan ke file users.txt
        echo "$username $password" >> users/users.txt
        break

    done

    break

done

